//
//  OpenCVWrapper.m
//  OpenCVDemo
//
//  Created by Jones Vitolo on 9/22/17.
//  Copyright © 2017 Jones Vitolo. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#import "OpenCVWrapper.h"

@implementation OpenCVWrapper

+ (NSString *) openCVVersionString {
    return [NSString stringWithFormat:@"OpenCV Version %s", CV_VERSION];
}

@end
