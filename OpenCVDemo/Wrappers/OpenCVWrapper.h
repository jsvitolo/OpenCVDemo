//
//  OpenCVWrapper.h
//  OpenCVDemo
//
//  Created by Jones Vitolo on 9/22/17.
//  Copyright © 2017 Jones Vitolo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenCVWrapper : NSObject
+ (NSString *) openCVVersionString;

@end
