//
//  ViewController.swift
//  OpenCVDemo
//
//  Created by Jones Vitolo on 9/22/17.
//  Copyright © 2017 Jones Vitolo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var openCVVersionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        openCVVersionLabel.text = OpenCVWrapper.openCVVersionString()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

